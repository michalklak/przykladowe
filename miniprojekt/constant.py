import numpy as np
from cv2 import cv2

dylacja = 'dylacja'
erozja = 'erozja'
otwarcie = 'otwarcie'
zamkniecie = 'zamknięcie'

elStruktKrzyzStr = "krzyż"
elStruktElipsaStr = "elipsa"
elStruktKwadratStr = "kwadrat"

elStruktKrzyz = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,5))
elStruktElipsa = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
elStruktKwadrat = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))

maxIntensywnoscPiksela = 255
minIntensywnoscPiksela = 0