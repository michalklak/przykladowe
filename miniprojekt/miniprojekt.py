import numpy as np
from cv2 import cv2
from matplotlib import pyplot as plt
import tkinter as tk
import tkinter.ttk as ttk
import os
import re
import constant

class Morfologia:

    def __init__(self):
        self.__obrazBinarny = []
        self.__tytulyObrazowDoRysowania = []
        self.__obrazyDoRysowania = []
        self.__obraz = []
        self.__elementStrukturalny = []
        self.__prog = 0

    def wczytajObraz(self, sciezka):
        self.__obraz = cv2.imread(sciezka,0) #wczytanie i przetworzenie na obraz w skali szarosci
        self.dodajDoRysowania('OBRAZ ORYGINALNY',self.__obraz)

    def ustawProg(self, prog):
        self.__prog = int(prog)

    def ustawElementStrukturalny(self, rodzaj):
        if rodzaj == constant.elStruktKrzyzStr:
            self.__elementStrukturalny = constant.elStruktKrzyz
        elif rodzaj == constant.elStruktElipsaStr:
            self.__elementStrukturalny = constant.elStruktElipsa
        elif rodzaj == constant.elStruktKwadratStr:
            self.__elementStrukturalny = constant.elStruktKwadrat
        self.dodajDoRysowania('ELEMENT STRUKTURALNY', self.__elementStrukturalny)
 
    def dodajDoRysowania(self, tytul, obraz):
        self.__tytulyObrazowDoRysowania.append(tytul)
        self.__obrazyDoRysowania.append(obraz)
    
    def progowanie(self):
        ret,binarny = cv2.threshold(self.__obraz, self.__prog, constant.maxIntensywnoscPiksela, cv2.THRESH_BINARY)
        self.__obrazBinarny = binarny
        self.dodajDoRysowania('PROGOWANIE', binarny)
    
    def histogram(self):
        histogram = cv2.calcHist([self.__obraz],[0],None,[256],[0,256])
        self.dodajDoRysowania("HISTOGRAM", histogram)

    def dylacja(self, lbIteracji):
        wynik = cv2.dilate(self.__obrazBinarny, self.__elementStrukturalny, iterations = lbIteracji)
        self.dodajDoRysowania('DYLACJA, {0} iter.'.format(lbIteracji), wynik)

    def erozja(self, lbIteracji):
        wynik = cv2.erode(self.__obrazBinarny, self.__elementStrukturalny, iterations = lbIteracji)
        self.dodajDoRysowania('EROZJA, {0} iter.'.format(lbIteracji), wynik)

    def otwarcie(self, lbIteracji):
        wynik = cv2.morphologyEx(self.__obrazBinarny, cv2.MORPH_OPEN, self.__elementStrukturalny)
        self.dodajDoRysowania('OTWARCIE, {0} iter.'.format(lbIteracji), wynik)

    def zamkniecie(self, lbIteracji):
        wynik = cv2.morphologyEx(self.__obrazBinarny, cv2.MORPH_CLOSE, self.__elementStrukturalny)
        self.dodajDoRysowania('ZAMKNIĘCIE, {0} iter.'.format(lbIteracji), wynik)

    def wykonajOperacjeMorfologiczna(self, operacja, lbIteracji):
        if operacja == constant.dylacja:
            self.dylacja(lbIteracji)
        elif operacja == constant.erozja:
            self.erozja(lbIteracji)
        elif operacja == constant.otwarcie:
            self.otwarcie(lbIteracji)
        elif operacja == constant.zamkniecie:
            self.zamkniecie(lbIteracji)

    def wykonajSekwencjeOperacjiMorf(self, sciezka, prog, elStrukt, operacja):
        self.wczytajObraz(sciezka)
        self.ustawProg(prog)
        self.ustawElementStrukturalny(elStrukt)
        self.histogram()
        self.progowanie()
        iteracje = []
        szerokoscObrazu = self.__obrazBinarny.shape[1]
        if szerokoscObrazu <= 1000:
            iteracje = [1,3,5,10]
        else:
            iteracje = [3,6,12,25]
        for i in iteracje:
            self.wykonajOperacjeMorfologiczna(operacja,i)
    
    def rysujWszystko(self):
        if len(self.__obrazyDoRysowania) == len(self.__tytulyObrazowDoRysowania):
            ile = len(self.__obrazyDoRysowania)
            plt.figure()
            plt.title("WYNIKI")
            for i in range(ile):
                plt.subplot(2,4,i+1)
                plt.title(self.__tytulyObrazowDoRysowania[i])
                plt.xticks([]), plt.yticks([])
                if i == 2: #HISTOGRAM
                    plt.plot(self.__obrazyDoRysowania[i])
                    plt.xlabel("intensywnosc piksela")
                    plt.ylabel("liczba pikseli")
                    plt.xticks(np.arange(0, 255, 48).tolist())
                else:
                    plt.imshow(self.__obrazyDoRysowania[i],'gray')
            plt.show()
        else:
            print('Rysowanie obrazów wynikowych nie powiodło się')

    def wypiszObrazy(self):
        print(self.__tytulyObrazowDoRysowania)

def walidujDane(sciezka, prog):
    if os.path.isfile(sciezka) == False:
        print("Niepoprawna ścieżka obrazu")
        return False
    if sciezka.lower().endswith(('.jpg', '.jpeg', '.png', '.bmp', '.gif', '.tiff')) == False:
        print("Podany plik nie jest obrazem")
        return False
    if not(prog.isdigit()):
        print("Podany próg musi być liczbą")
        return False
    if int(prog) < 0 or int(prog) > 255:
        print("Wartosc progu do binaryzacji musi miescic sie w zakresie od 0 do 255")
        return False
    return True

def generujOkno():

    window = tk.Tk()
    window.maxsize(480,210)
    window.geometry('480x210')
    window.title("Morfologia obrazów")

    lbPlik = tk.Label(window, text="Podaj ścieżkę pliku obrazu:")
    lbPlik.grid(column = 0, row = 0)
    lbProg = tk.Label(window, text="Podaj próg binaryzacji (0-255):")
    lbProg.grid(column = 0, row = 2)
    lbElStrukt = tk.Label(window, text="Wybierz element strukturalny:")
    lbElStrukt.grid(column = 0, row = 4)
    lbOperacja = tk.Label(window, text="Wybierz operację:")
    lbOperacja.grid(column = 0, row = 6)

    tbPlik = tk.Entry(window, width = 80)
    tbPlik.grid(column = 0, row = 1)
    tbProg = tk.Entry(window, width=20)
    tbProg.grid(column = 0, row = 3)
    tbProg.insert(0,'125')

    cbElStrukt = ttk.Combobox(window)
    cbElStrukt['values']= (constant.elStruktKwadratStr, constant.elStruktElipsaStr, constant.elStruktKrzyzStr)
    cbElStrukt.current(0)
    cbElStrukt.grid(column=0, row=5)
    cbOperacja = ttk.Combobox(window)
    cbOperacja['values']= (constant.dylacja, constant.erozja, constant.otwarcie, constant.zamkniecie)
    cbOperacja.current(0)
    cbOperacja.grid(column=0, row=7)

    def klik():
        sciezka = tbPlik.get()
        prog = tbProg.get()
        if walidujDane(sciezka, prog):
            elStrukt = cbElStrukt.get()
            operacja = cbOperacja.get()
            morfologia = Morfologia()
            morfologia.wykonajSekwencjeOperacjiMorf(sciezka, prog, elStrukt, operacja)
            morfologia.rysujWszystko()
        
    btnRysuj = tk.Button(window, text="RYSUJ", bg='beige', command = klik)
    btnRysuj.grid(column=0, row=8, pady=(10,10))

    return window

window = generujOkno()
window.mainloop()